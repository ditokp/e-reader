/*
 * Copyright (C) 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package nightlock.peppercarrot.adapters

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.mikepenz.aboutlibraries.LibsBuilder
import nightlock.peppercarrot.R
import nightlock.peppercarrot.fragments.AboutFragment
import java.lang.IllegalArgumentException

class AboutViewPagerAdapter(fragmentManager: FragmentManager, val context: Context) :
        FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment = when (position) {
        0    -> AboutFragment()
        1    -> LibsBuilder().supportFragment()
        else -> throw IllegalArgumentException(
                "Unknown index \"$position\" given in AboutViewPagerAdapter")
    }

    override fun getCount(): Int = 2

    override fun getPageTitle(position: Int): CharSequence = when (position) {
        0    -> context.getString(R.string.about_menu)
        1    -> context.getString(R.string.about_license)
        else -> throw IllegalArgumentException(
                "Unknown index \"$position\" given in AboutViewPagerAdapter")
    }

}
