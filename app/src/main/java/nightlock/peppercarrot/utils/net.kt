/*
 * Copyright (C) 2017 - 2018 Jihoon Kim <imsesaok@gmail.com, imsesaok@tuta.io>
 *
 * This file is part of Reader for Pepper&Carrot.
 *
 * Reader for Pepper&Carrot is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nightlock.peppercarrot.utils

import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONArray
import org.json.JSONObject
import javax.net.ssl.SSLHandshakeException

/**
 * Utility functions for network tasks.
 * Created by Jihoon Kim on 2017-05-03.
 */

@Throws(SSLHandshakeException::class)
fun getEpisodeList(): List<Episode> {
    val episodeList = ArrayList<Episode>()
    val rawEpisodeList = getFromUrl("${ROOT_URL}episodes.json")
    val episodeJson = JSONArray(rawEpisodeList)

    for (i in 0 until episodeJson.length()) {
        val episode = episodeJson.getJSONObject(i)

        val name = episode.getString("name")
        val page = episode.getInt("total_pages")
        val languageJsonArray = episode.getJSONArray("translated_languages")

        val pncLanguageList = ArrayList<String>()
        for (j in 0 until languageJsonArray.length())
            pncLanguageList += languageJsonArray.getString(j)

        episodeList += Episode(i, name, page, pncLanguageList)
    }
    return episodeList
}

@Throws(SSLHandshakeException::class)
fun getLanguageList(): List<Language> {
    val languageList = ArrayList<Language>()
    val rawLangList =
        getFromUrl("https://qtwyeuritoiy.github.io/peppercarrot-metadata/lang/lang.json")
    val langJson = JSONObject(rawLangList)

    for (pncLanguage in langJson.keys()) {
        val langObject: JSONObject = langJson.getJSONObject(pncLanguage)
        val translatorsJsonArray = langObject.getJSONArray("translators")
        val name: String = langObject.getString("name")
        val localName: String = langObject.getString("local_name")
        val isoCode: String = langObject.getString("iso_code")
        val isoVersion: Int = langObject.getInt("iso_version")

        val translators = ArrayList<String>()
        for (j in 0 until translatorsJsonArray.length())
            translators += translatorsJsonArray.getString(j)

        languageList += Language(name, localName, translators, pncLanguage, isoCode, isoVersion)
    }
    return languageList
}

fun thisOrThat(vararg urls: String): String? {
    for (url in urls)
        if (pokeAt(url)) return url
    return null
}

@Throws(SSLHandshakeException::class)
fun getFromUrl(url: String): String {
    val client = OkHttpClient()
    val request = Request.Builder()
            .url (url)
            .build ()

    val response = client.newCall(request).execute()
    return response.body ()!!.string ()
}

fun pokeAt(url: String): Boolean {
    val client = OkHttpClient()
    val request = Request.Builder()
            .url(url)
            .build()

    val response = client.newCall(request).execute()
    return response.isSuccessful
}
