Reader for Pepper&Carrot ("the APP") *does not* store or transmit personal information.

The APP connects to third-party sites over the internet; [Sources](https://www.peppercarrot.com/0_sources/) page of the [Official Pepper&Carrot Website](https://www.peppercarrot.com/), and the [lang.json File](https://qtwyeuritoiy.github.io/peppercarrot-metadata/lang/lang.json) on [GitHub](https://github.com/).
The APP connects to these (and *only* these and its sub-directories) sites *only* to serve the core purpose of the APP.
No personal data are transmitted in the process, and the APP does not handle cookies.
Refer to the Privacy Policy of each site for more information.

This document will be updated promptly whenever a change has happened to how the APP handles your personal data.

The APP will notify you of the change to this document whenever the change has occured, and the change is reflected in the APP.